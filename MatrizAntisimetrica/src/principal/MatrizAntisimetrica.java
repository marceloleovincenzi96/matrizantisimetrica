package principal;

public class MatrizAntisimetrica {
	
	public boolean esCuadradaAntisimetrica (int [][] matriz) {
		
		boolean esCuadradaAntisimetrica = true;
		
		if (matriz.length == matriz [0].length) {
			
			for (int i = 0; i < matriz.length && esCuadradaAntisimetrica; i++) {
				
				for (int j = 0; j < matriz[i].length && esCuadradaAntisimetrica; j++) {
					
					if (matriz [i][j] != -(matriz[j][i])) {
						
						esCuadradaAntisimetrica = false;
						
					}
				}
			}
			
		} 
		
		return esCuadradaAntisimetrica;
	}

}
